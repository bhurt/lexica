{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Lexica.Internal.RangeMap (
    RangeMap,
    toAscList,
    empty,
    singleton,
    lookup,
    union,
    intersection,
    difference,
    simplify
) where

    import           Control.DeepSeq
    import           Data.Hashable
    import           Data.These
    import           GHC.Generics
    import           Lexica.Internal.Range
    import           Prelude               hiding (lookup)

    newtype RangeMap k v = RangeMap { toAscList :: [ (Range k, v) ] }
        deriving (Show, Read, Ord, Eq, Generic)

    instance Functor (RangeMap k) where
        fmap f rm = RangeMap $ fmap f <$> toAscList rm

    instance (NFData k, NFData v) => NFData (RangeMap k v) where
        rnf = rnf . toAscList 

    instance (Hashable k, Hashable v) => Hashable (RangeMap k v) where
        hashWithSalt s rm = hashWithSalt s (toAscList rm)

    empty :: RangeMap k v
    empty = RangeMap []

    singleton :: Range k -> v -> RangeMap k v
    singleton r v = RangeMap [ (r, v) ]

    lookup :: forall k v . Ord k => k -> RangeMap k v -> Maybe v
    lookup k rm = foldr go Nothing (toAscList rm)
        where
            go :: (Range k, v) -> Maybe v -> Maybe v
            go (r, v) m
                | r `contains` k = Just v
                | otherwise      = m

    combine :: forall a b k .
                (Ord k , Enum k)
                => [ (Range k, a) ]
                -> [ (Range k, b) ]
                -> [ (Range k, These a b) ]
    combine [] [] = []
    combine xs [] = fmap This <$> xs
    combine [] ys = fmap That <$> ys
    combine xxs@((xr,xv):xs) yys@((yr,yv):ys) =
        let xl = lowerBound xr
            xu = upperBound xr
            yl = lowerBound yr
            yu = upperBound yr
        in
        if  
            -- Do the ranges even overlap?  If the upper bound of one is
            -- strictly less than the lower bound of the other, then no,
            -- the don't overlap.  In which case the lower wins.
            | (xu < yl) -> (xr, This xv) : combine xs yys
            | (yu < xl) -> (yr, That yv) : combine xxs ys

            -- Now the ranges overlap.  Does one start before the other?
            -- In which case we pick of the start of that side.
            | (xl < yl) ->
                (makeRange xl (pred yl), This xv)
                    : combine ((makeRange yl xu, xv) : xs) yys
            | (yl < xl) ->
                (makeRange yl (pred xl), That yv)
                    : combine xxs ((makeRange xl yu, yv) : ys)

            -- Both ranges start at the same value (xl == yl).  Is one
            -- shorter than the other?
            | (xu < yu) ->
                (xr, These xv yv)
                    : combine xs ((makeRange (succ xu) yu, yv) : ys)
            | (yu < xu) ->
                (yr, These xv yv)
                    : combine ((makeRange (succ yu) xu, xv) : xs) ys

            -- Both ranges are identical (xl == yl && xu == yu).
            | otherwise -> (xr, These xv yv) : combine xs ys

    union :: forall k a b .
                (Ord k, Enum k)
                => RangeMap k a
                -> RangeMap k b
                -> RangeMap k (These a b)
    union ra rb = RangeMap $ combine (toAscList ra) (toAscList rb)

    intersection :: forall k a b .
                    (Ord k, Enum k)
                    => RangeMap k a
                    -> RangeMap k b
                    -> RangeMap k (a, b)
    intersection ra rb = RangeMap res
        where
            res :: [ (Range k, (a, b)) ]
            res = foldr go [] comb

            comb :: [ (Range k, These a b) ]
            comb = combine (toAscList ra) (toAscList rb)

            go :: (Range k, These a b)
                    -> [ (Range k, (a, b)) ]
                    -> [ (Range k, (a, b)) ]
            go (_, This _)    xs = xs
            go (_, That _)    xs = xs
            go (r, These x y) xs = (r, (x, y)) : xs

    difference :: forall k a b .
                    (Ord k, Enum k)
                    => RangeMap k a
                    -> RangeMap k b
                    -> RangeMap k a
    difference ra rb = RangeMap res
        where
            res :: [ (Range k, a) ]
            res = foldr go [] comb

            comb :: [ (Range k, These a b) ]
            comb = combine (toAscList ra) (toAscList rb)

            go :: (Range k, These a b)
                    -> [ (Range k, a) ]
                    -> [ (Range k, a) ]
            go (r, This a) xs = (r, a) : xs
            go (_, _     ) xs = xs


    simplify :: forall k a .
                (Ord k, Enum k, Eq a)
                => RangeMap k a
                -> RangeMap k a
    simplify ra = RangeMap $ go (toAscList ra)
        where
            go :: [ (Range k, a) ] -> [ (Range k, a) ]
            go [] = []
            go [x] = [x]
            go ((r1, a1) : (r2, a2) : xs)
                | (succ(upperBound r1) == (lowerBound r2)) && (a1 == a2) =
                    go ((makeRange (lowerBound r1) (upperBound r2), a1) : xs)
                | otherwise = (r1, a1) : go ((r2, a2) : xs)

