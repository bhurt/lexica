{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE ScopedTypeVariables #-}


module Lexica.Internal.Range (
    Range,
    makeRange,
    lowerBound,
    upperBound,
    contains
) where

    import           Control.DeepSeq
    import           Data.Hashable
    import           GHC.Generics
    import           Prelude         hiding (lookup)

    data Range a =
        Single a
        | Range a a
        deriving (Show, Read, Ord, Eq, Generic)

    instance NFData a => NFData (Range a) where
        rnf (Single a) = rnf a
        rnf (Range a b) = rnf a `seq` rnf b

    instance Hashable a => Hashable (Range a) where
        hashWithSalt s (Single a) = 
            s `hashWithSalt` a `hashWithSalt` (1 :: Int) 
        hashWithSalt s (Range a b) =
            s `hashWithSalt` a `hashWithSalt` b `hashWithSalt` (2 :: Int)

    makeRange :: Ord a => a -> a -> Range a
    makeRange lo hi = case compare lo hi of
                        EQ -> Single lo
                        LT -> Range lo hi
                        GT -> Range hi lo

    
    lowerBound :: Range a -> a
    lowerBound (Single a)  = a
    lowerBound (Range a _) = a

    upperBound :: Range a -> a
    upperBound (Single a)  = a
    upperBound (Range _ a) = a

    contains :: Ord a => Range a -> a -> Bool
    contains (Single a) b = a == b
    contains (Range a b) c = (a <= c) && (c <= b)


