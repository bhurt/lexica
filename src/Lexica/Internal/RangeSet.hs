{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Lexica.Internal.RangeSet (
    RangeSet,
    toAscList,
    empty,
    singleton,
    member,
    union,
    intersection,
    negation,
    simplify
) where

    import           Control.DeepSeq
    import           Data.Hashable
    import           GHC.Generics
    import           Lexica.Internal.Range

    newtype RangeSet k = RangeSet { toAscList :: [ Range k ] }
        deriving (Show, Read, Ord, Eq, Generic)

    instance NFData k => NFData (RangeSet k) where
        rnf (RangeSet xs) = rnf xs `seq` ()

    instance Hashable k => Hashable (RangeSet k) where
        hashWithSalt s rs = hashWithSalt s (toAscList rs)

    empty :: RangeSet k
    empty = RangeSet []

    singleton :: Range k -> RangeSet k
    singleton r = RangeSet [ r ]

    member :: forall k . Ord k => k -> RangeSet k -> Bool
    member k rset = loop (toAscList rset)
        where
            loop :: [ Range k ] -> Bool
            loop [] = False
            loop (r : rs) = (r `contains` k) || loop rs

    simplify :: forall k . (Ord k, Enum k)
                => RangeSet k -> RangeSet k
    simplify rs = RangeSet $ loop (toAscList rs)
        where
            loop :: [ Range k ] -> [ Range k ]
            loop [] = []
            loop [r] = [r]
            loop (r1 : r2 : rss)
                | upperBound r1 < (pred (lowerBound r2)) =
                    r1 : loop (r2 : rss)
                | otherwise =
                    loop (makeRange (lowerBound r1) (upperBound r2) : rss)

    union :: forall k . (Ord k, Enum k)
                => RangeSet k -> RangeSet k -> RangeSet k
    union rs1 rs2 = RangeSet $ loop (toAscList rs1) (toAscList rs2)
        where
            loop :: [ Range k ] -> [ Range k ] -> [ Range k ]
            loop []     ss     = ss
            loop rs     []     = rs
            loop (r:rs) (s:ss)
                | upperBound r < lowerBound s = r : loop rs (s : ss)
                | upperBound s < lowerBound r = s : loop (r:rs) ss
                | lowerBound r < lowerBound s =
                    makeRange (lowerBound r) (pred (lowerBound s))
                        : loop
                            (makeRange (lowerBound s) (upperBound r) : rs)
                            (s : ss)
                | lowerBound s < lowerBound r =
                    makeRange (lowerBound s) (pred (lowerBound r))
                        : loop 
                            (r : rs)
                            (makeRange (lowerBound r) (upperBound s) : ss)
                | upperBound r < upperBound s =
                    r : loop
                            rs
                            (makeRange (succ (upperBound r)) (upperBound s)
                                : ss)
                | upperBound s < upperBound r =
                    s : loop
                            (makeRange (succ (upperBound s)) (upperBound r)
                                : rs)
                            ss
                | otherwise = r : loop rs ss

    intersection :: forall k . (Ord k, Enum k)
                    => RangeSet k -> RangeSet k -> RangeSet k
    intersection rs1 rs2 = RangeSet $ loop (toAscList rs1) (toAscList rs2)
        where
            loop :: [ Range k ] -> [ Range k ] -> [ Range k ]
            loop []     _      = []
            loop _      []     = []
            loop (r:rs) (s:ss)
                | upperBound r < lowerBound s = loop rs (s : ss)
                | upperBound s < lowerBound r = loop (r:rs) ss
                | lowerBound r < lowerBound s =
                    loop
                        (makeRange (lowerBound s) (upperBound r) : rs)
                        (s : ss)
                | lowerBound s < lowerBound r =
                    loop 
                        (r : rs)
                        (makeRange (lowerBound r) (upperBound s) : ss)
                | upperBound r < upperBound s =
                    r : loop
                            rs
                            (makeRange (succ (upperBound r)) (upperBound s)
                                : ss)
                | upperBound s < upperBound r =
                    s : loop
                            (makeRange (succ (upperBound s)) (upperBound r)
                                : rs)
                            ss
                | otherwise = r : loop rs ss


    negation :: forall k . (Ord k, Enum k, Bounded k)
                => RangeSet k -> RangeSet k
    negation rset = RangeSet $ prefix (toAscList rset)
        where
            prefix :: [ Range k ] -> [ Range k ]
            prefix [] = [ makeRange minBound maxBound ]
            prefix (r : rs)
                | minBound < lowerBound r =
                    makeRange minBound (pred (lowerBound r))
                        : loop (upperBound r) rs
                | otherwise = loop (upperBound r) rs

            loop :: k -> [ Range k ] -> [ Range k ]
            loop limit []
                | limit < maxBound = [ makeRange (succ limit) maxBound  ]
                | otherwise        = []
            loop limit (r : rs)
                | succ limit < lowerBound r =
                    makeRange (succ limit) (pred (lowerBound r))
                        : loop (upperBound r) rs
                | otherwise = loop (upperBound r) rs
