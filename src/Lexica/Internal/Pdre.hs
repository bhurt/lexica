{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Lexica.Internal.Pdre(
    Pdre(..),
    token,
    singleRange,
    literal,
    isAccepting,
    isError,
    activeGroupings,
    acceptingGroupings,
    simplify,
    transition
) where

    import           Control.DeepSeq
    import           Data.Hashable
    import           Data.Map.Strict            (Map)
    import qualified Data.Map.Strict            as Map
    import           Data.Set                   (Set)
    import qualified Data.Set                   as Set
    import           GHC.Generics
    import           Lexica.Internal.HasBuilder
    import           Lexica.Internal.Range      (Range)
    import qualified Lexica.Internal.Range      as Range
    import           Lexica.Internal.RangeSet   (RangeSet)
    import qualified Lexica.Internal.RangeSet   as RangeSet

    data Pdre b k t =
        -- | The empty set.
        Error
        -- } The set containing only the empty string (\"\").
        | Accepting
        -- | The set containing all strings, including the empty string.
        | All
        -- | The set containing all strings, except the empty string
        | Nonempty

        -- | The set of single-character strings, for all characters in
        -- a given set.
        | Match (RangeSet t)

        | Follow (Pdre b k t) (Pdre b k t)
        | Rep Word (Maybe Word) (Pdre b k t)

        | And (Pdre b k t) (Pdre b k t)
        | Or (Pdre b k t) (Pdre b k t)
        | Not (Pdre b k t)

        | Grouping k (Pdre b k t)
        | Backref b k

        deriving (Show, Read, Ord, Eq, Generic)

    instance (NFData b, NFData k, NFData t) => NFData (Pdre b k t) where
        rnf Error          = ()
        rnf Accepting      = ()
        rnf All            = ()
        rnf Nonempty       = ()
        rnf (Match s)      = rnf s `seq` ()
        rnf (Follow x y)   = rnf x `seq` rnf y `seq` ()
        rnf (Rep x y z)    = rnf x `seq` rnf y `seq` rnf z `seq` ()
        rnf (And x y)      = rnf x `seq` rnf y `seq` ()
        rnf (Or  x y)      = rnf x `seq` rnf y `seq` ()
        rnf (Not x)        = rnf x `seq` ()
        rnf (Grouping k p) = rnf k `seq` rnf p `seq` ()
        rnf (Backref r k)  = rnf r `seq` rnf k `seq` ()

    instance (Hashable b, Hashable k, Hashable t)
        => Hashable (Pdre b k t)
        where
            hashWithSalt s Error          = hashWithSalt s (1 :: Int)
            hashWithSalt s Accepting      = hashWithSalt s (2 :: Int)
            hashWithSalt s All            = hashWithSalt s (3 :: Int)
            hashWithSalt s Nonempty       = hashWithSalt s (4 :: Int)
            hashWithSalt s (Match rs)     =
                s `hashWithSalt` rs `hashWithSalt` (5 :: Int)
            hashWithSalt s (Follow p1 p2) =
                s `hashWithSalt` p1 `hashWithSalt` p2 `hashWithSalt` (6 :: Int)
            hashWithSalt s (Rep lo up p)  =
                s `hashWithSalt` lo `hashWithSalt` up
                    `hashWithSalt` p `hashWithSalt` (7 :: Int)
            hashWithSalt s (And p1 p2)    =
                s `hashWithSalt` p1 `hashWithSalt` p2 `hashWithSalt` (8 :: Int)
            hashWithSalt s (Or p1 p2)     =
                s `hashWithSalt` p1 `hashWithSalt` p2 `hashWithSalt` (9 :: Int)
            hashWithSalt s (Not p)        =
                s `hashWithSalt` p `hashWithSalt` (10 :: Int)
            hashWithSalt s (Grouping k p) =
                s `hashWithSalt` k `hashWithSalt` p `hashWithSalt` (11 :: Int)
            hashWithSalt s (Backref b k) =
                s `hashWithSalt` b `hashWithSalt` k `hashWithSalt` (12 :: Int)

    token :: Ord t => t -> Pdre b k t
    token t = Match . RangeSet.singleton $ Range.makeRange t t

    singleRange :: Range t -> Pdre b k t
    singleRange r = Match $ RangeSet.singleton r

    literal :: Ord t => [ t ] -> Pdre b k t
    literal [] = Accepting
    literal [x] = token x
    literal (x:xs) = Follow (token x) (literal xs)

    isAccepting :: (Ord k, HasBuilder r)
                    => Map k r -> Pdre b k (Token r) -> Bool
    isAccepting _ Error           = False
    isAccepting _ Accepting       = True
    isAccepting _ All             = True
    isAccepting _ Nonempty        = False
    isAccepting _ (Match _)       = False
    isAccepting m (Follow p1 p2)  = isAccepting m p1 && isAccepting m p2
    isAccepting m (Rep lo _ p)    = (lo == 0) || (isAccepting m p)
    isAccepting m (And p1 p2)     = isAccepting m p1 && isAccepting m p2
    isAccepting m (Or p1 p2)      = isAccepting m p1 || isAccepting m p2
    isAccepting m (Not p)         = not (isAccepting m p)
    isAccepting m (Grouping _ p)  = isAccepting m p
    isAccepting m (Backref _ k)   = 
        case Map.lookup k m of
            Nothing -> False
            Just r  -> isEmpty r

    isError :: (Ord k, HasBuilder r)
                => Map k r -> Pdre b k (Token r) -> Bool
    isError _ Error           = True
    isError _ Accepting       = False
    isError _ All             = False
    isError _ Nonempty        = False
    isError _ (Match _)       = False
    isError m (Follow p1 p2)  = isError m p1 || isError m p2
    isError m (Rep _ _ p)     = isError m p
    isError m (And p1 p2)     = isError m p1 || isError m p2
    isError m (Or p1 p2)      = isError m p1 && isError m p2
    isError m (Not p1)        = not (isError m p1)
    isError m (Grouping _ p)  = isError m p
    isError m (Backref _ k)   =
        case Map.lookup k m of
            Nothing -> True
            Just _  -> False

    -- How we handle groupings and back references:
    --
    -- The active groupings of a Pdre are those groupings which
    -- do not require some non-empty sequence of tokens to be
    -- consumed before they start.  The activeGroupings function
    -- gives us the set of active groupings (by key).
    --
    -- So now we can compare the active groupings before and after
    -- a state transition (which consumes a token from the input
    -- stream).
    --  - If the grouping was active both before and after the transition,
    --      we just append the consumed token to the builder for
    --      that grouping.
    --  - If the grouping was active before, but not active after the
    --      transition, then if it is accepting, which is to say
    --      that there is some instance of the grouping where the
    --      child Pdre is accepting, then the grouping has completed
    --      successfully.  It's builder is converted into a result,
    --      and that grouping key/result pair is added to the map
    --      of completed groupings.  If there is no accepting grouping
    --      then the grouping failed.  It is removed from the currectly
    --      active groupings, but no completed grouping is added.
    --  - If the grouping was not active before, but is active after the
    --      transition, then the grouping has just started.  We add
    --      and empty builder to the map of active grouping keys to
    --      builders.  Note that the token consumed is NOT added to
    --      the builder!
    --  - If the grouping was not active both before and after the
    --      transition, we can ignore it.
    --
    -- We then have a map of completed grouping keys to results.
    -- Conceptually, when we hit a back reference in a derive,
    -- we just look it's grouping key up in the map, and replace
    -- the backref with a literal of the result.
    activeGroupings :: (Ord k, HasBuilder r)
                        => Map k r -> Pdre b k (Token r) -> Set k
    activeGroupings _ Error              = Set.empty
    activeGroupings _ Accepting          = Set.empty
    activeGroupings _ All                = Set.empty
    activeGroupings _ Nonempty           = Set.empty
    activeGroupings _ (Match _)          = Set.empty
    activeGroupings m (Follow p1 p2)
        | isAccepting m p1               = Set.union (activeGroupings m p1)
                                                        (activeGroupings m p2)
        | otherwise                    = activeGroupings m p1
    activeGroupings _ (Rep 0 (Just 0) _) = Set.empty
    activeGroupings m (Rep _ _ p)        = activeGroupings m p
    activeGroupings m (And p1 p2)        = Set.union (activeGroupings m p1)
                                                    (activeGroupings m p2)
    activeGroupings m (Or p1 p2)         = Set.union (activeGroupings m p1)
                                                    (activeGroupings m p2)
    activeGroupings m (Not p)            = activeGroupings m p
    activeGroupings m (Grouping k p)     = Set.insert k (activeGroupings m p)
    activeGroupings _ (Backref _ _)      = Set.empty

    acceptingGroupings :: (Ord k, HasBuilder r)
                        => Map k r -> Pdre b k (Token r) -> Set k
    acceptingGroupings _ Error              = Set.empty
    acceptingGroupings _ Accepting          = Set.empty
    acceptingGroupings _ All                = Set.empty
    acceptingGroupings _ Nonempty           = Set.empty
    acceptingGroupings _ (Match _)          = Set.empty
    acceptingGroupings m (Follow p1 p2)
        | isAccepting m p1               =
            Set.union (acceptingGroupings m p1) (acceptingGroupings m p2)
        | otherwise                    = acceptingGroupings m p1
    acceptingGroupings _ (Rep 0 (Just 0) _) = Set.empty
    acceptingGroupings m (Rep _ _ p)        = acceptingGroupings m p
    acceptingGroupings m (And p1 p2)        =
        Set.union (acceptingGroupings m p1) (acceptingGroupings m p2)
    acceptingGroupings m (Or p1 p2)         =
        Set.union (acceptingGroupings m p1) (acceptingGroupings m p2)
    acceptingGroupings m (Not p)            = acceptingGroupings m p
    acceptingGroupings m (Grouping k p)     
        | isAccepting m p                   =
            Set.insert k (acceptingGroupings m p)
        | otherwise                         =
            acceptingGroupings m p
    acceptingGroupings _ (Backref _ _)      = Set.empty


    -- Push down the Nots, pull up the base cases.  Don't try to be
    -- tricky simplifying the Ands and Ors.  This is just a fast pass
    -- over the data to reduce downstream work.
    simplify :: (Ord k, HasBuilder r)
                    => Map k r -> Pdre b k (Token r) -> Pdre b k (Token r)
    simplify _ Error                 = Error
    simplify _ Accepting             = Accepting
    simplify _ All                   = All
    simplify _ Nonempty              = Nonempty
    simplify _ (Match r)             = Match r
    simplify m (Follow p1 p2)        =
        case (simplify m p1, simplify m p2) of
            (Error,     _)         -> Error
            (Accepting, p3)        -> p3
            (_,         Error)     -> Error
            (p3,        Accepting) -> p3
            (p3,        p4)        -> Follow p3 p4
    simplify _ (Rep 0 (Just 0) _)    = Accepting
    simplify m (Rep lo up p)         = Rep lo up (simplify m p)
    simplify m (And p1 p2)           =
        case (simplify m p1, simplify m p2) of
            (Error, _)             -> Error
            (_, Error)             -> Error
            (Accepting, p3)
                | isAccepting m p3 -> Accepting
                | otherwise        -> Error
            (p3, Accepting)
                | isAccepting m p3 -> Accepting
                | otherwise        -> Error
            (p3, All)              -> p3
            (All, p3)              -> p3
            (p3, Nonempty)
                | isAccepting m p3 -> And p3 Nonempty
                | otherwise        -> p3
            (Nonempty, p3)
                | isAccepting m p3 -> And p3 Nonempty
                | otherwise        -> p3
            (p3, p4)               -> And p3 p4
    simplify m (Or p1 p2)            =
        case (simplify m p1, simplify m p2) of
            (All, _)                -> All
            (_, All)                -> All
            (Nonempty, p3)
                | isAccepting m p3  -> All
                | otherwise         -> Nonempty
            (p3, Nonempty)
                | isAccepting m p3  -> All
                | otherwise         -> Nonempty
            (Error, p3)             -> p3
            (p3, Error)             -> p3
            (Accepting, p3)
                | isAccepting m p3  -> p3
                | otherwise         -> Or Accepting p3
            (p3, Accepting)
                | isAccepting m p3  -> p3
                | otherwise         -> Or Accepting p3
            (p3, p4)                -> Or p3 p4

    simplify _ (Not Error)           = All
    simplify _ (Not Accepting)       = Nonempty
    simplify _ (Not All)             = Error
    simplify _ (Not Nonempty)        = Accepting
    simplify m (Not (And p1 p2))     = simplify m (Or (Not p1) (Not p2))
    simplify m (Not (Or p1 p2))      = simplify m (And (Not p1) (Not p2))
    simplify m (Not (Not p1))        = simplify m p1
    simplify m (Not p1)              = Not $ simplify m p1
    simplify m (Grouping k p)        = 
        case simplify m p of
            Error -> Error
            p1    -> Grouping k p1
    simplify _ (Backref b k)         = Backref b k

    transition :: (Ord k, Ord (Token r), HasBuilder r)
                    => Map k r
                    -> Token r
                    -> Pdre b k (Token r)
                    -> Pdre b k (Token r)
    transition _ _ Error           = Error
    transition _ _ Accepting       = Error
    transition _ _ All             = All
    transition _ _ Nonempty        = All
    transition _ t (Match s)
        | RangeSet.member t s      = Accepting
        | otherwise                = Error
    transition m t (Follow p1 p2 )
        | isAccepting m p1         = Or (Follow (transition m t p1) p2)
                                            (transition m t p2)
        | otherwise                = Follow (transition m t p1) p2
    transition m t (Rep lo up p)
        | lo == 0                  =
            case up of
                Nothing  -> transition m t (Follow p (Rep 0 Nothing p))
                (Just 0) -> Error
                (Just n) -> transition m t (Follow p (Rep 0 (Just (n-1)) p))
        | otherwise                = transition m t (Follow p (Rep (lo-1) up p))
    transition m t (And p1 p2)     = And (transition m t p1) (transition m t p2)
    transition m t (Or p1 p2)      = Or (transition m t p1) (transition m t p2)
    transition m t (Not p)         = Not (transition m t p)
    transition m t (Grouping k p)  = Grouping k (transition m t p)
    transition m t (Backref _ k)   =
        case Map.lookup k m of
            Nothing -> Error
            Just r  -> transition m t (literal (unpack r))

{-
    apply :: forall t . Ord t => t -> Pdre t -> Maybe (Pdre t)
    apply _ Error              = Error
    apply _ Accepting          = Error
    apply _ All                = All
    apply _ Nonempty           = All

    apply t (Match s)
        | RangeSet.member t s  = Accepting
        | otherwise            = Error

    apply t (Follow p1 p2)      
        | isAccepting p1       = Or (Follow (apply t p1) p2) (apply t p2)
        | otherwise            = Follow (apply t p1) p2

    apply t (Rep 0 Nothing  p) = Follow (apply t p1) (Rep 0 Nothing p)
    apply _ (Rep 0 (Just 0) _) = Error
    apply t (Rep 0 (Just 1) p) = apply t p
    apply t (Rep n k        p) = Follow (apply t p) (Rep (n-1) k p)

    apply t (And p1 p2)        = And (apply t p1) (apply t p2)
    apply t (Or p1 p2)         = Or (apply t p1) (apply t p2)
    apply t (Not p)            = Not (apply t p)

    reduce :: forall t . (Ord t, Enum r, Bounded t) =>
                Pdre t -> Pdre t
    reduce = phase2 . phase1
        where

            -- Phase 1: push nots down as far as they'll go.
            phase1 :: Pdre t -> Pdre t
            phase1 Error              = Error
            phase1 Accepting          = Accepting
            phase1 All                = All
            phase1 Nonempty           = Nonempty
            phase1 (Match s)          = Match s
            phase1 (Follow p1 p2)     = Follow (phase1 p1) (phase1 p2)
            phase1 (Rep 0 (Just 0) _) = Error
            -- TODO: more Rep reductions here
            phase1 (Rep n k        p) = Rep n k (phase1 p)
            phase1 (And p1 p2)        = And (phase1 p1) (phase1 p2)
            phase1 (Or p1 p2)         = Or (phase1 p1) (phase1 p2)
            phase1 (Not p)            = quash p

            quash :: Pdre t -> Pdre t
            quash Error              = All
            quash Accepting          = Nonempty
            quash All                = Error
            quash Nonempty           = Accepting
            quash (Match s)          = Match $ RangeSet.negation s
            quash (Follow p1 p2)     = Not $ Follow (phase1 p1) (phase1 p2)
            quash (Rep 0 (Just 0) _) = All
            -- TODO: more Rep reductions here
            quash (Rep n k        o) = Not $ Rep n k (phase1 p)
            quash (And p1 p2)        = Or (quash p1) (quash p2)
            quash (Or p1 p2)         = And (quash p1) (quash p2)
            quash (Not p)            = phase1 p

            phase2 :: Pdre t -> Pdre t
            phase2 Error       = Error
            phase2 Accepting   = Accepting
            phase2 All         = All
            phase2 Nonempty    = Nonempty
            phase2 (Match s)   = Match s
            phase2 (Follow p1 p2) = optF $ flatF p1 $ flatF p2 []
            phase2 (Rep n k p) = Rep n k (phase2 p)
            phase2 _ = undefined
            -- TODO: Finish this.

            flatF :: Pdre t -> [ Pdre t] -> [ Pdre t ]
            flatF (Follow p1 p2) fs = flatF p1 $ flatF p2 fs
            flatF p fs              = (phase2 p) : fs

            optF :: [ Pdre t ] -> Pdre t
            optF xs  = foldr1 Follow xs

    nextSet :: Pdre t -> RangeSet t
    nextSet = undefined

-}
