{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Lexica.Internal.HasBuilder(
    HasBuilder(..)
) where

    import           Data.ByteString         (ByteString)
    import qualified Data.ByteString         as Bytes
    import qualified Data.ByteString.Builder as BytesBuilder
    import qualified Data.ByteString.Lazy    as LazyBytes
    import           Data.Kind               (Type)
    import           Data.Proxy
    import           Data.Text               (Text)
    import qualified Data.Text               as Text
    import qualified Data.Text.Lazy          as LazyText
    import qualified Data.Text.Lazy.Builder  as TextBuilder
    import           Data.Word               (Word8)

    class HasBuilder r where
        type Token r :: Type
        type Builder r :: Type

        newBuilder :: Proxy r -> Builder r
        appendBuilder :: Proxy r -> Builder r -> Token r -> Builder r
        finishBuilder :: Builder r -> r
        unpack :: r -> [ Token r ]
        isEmpty :: r -> Bool

    instance HasBuilder [a] where
        type Token [a] = a
        type Builder [a] = [a]

        newBuilder Proxy = []
        appendBuilder Proxy b t = t : b
        finishBuilder = reverse
        unpack = id
        isEmpty [] = True
        isEmpty _  = False

    instance HasBuilder Text where
        type Token Text = Char
        type Builder Text = TextBuilder.Builder

        newBuilder Proxy = mempty
        appendBuilder Proxy b c = b <> TextBuilder.singleton c
        finishBuilder = LazyText.toStrict . TextBuilder.toLazyText
        unpack = Text.unpack
        isEmpty = Text.null

    instance HasBuilder LazyText.Text where
        type Token LazyText.Text = Char
        type Builder LazyText.Text = TextBuilder.Builder

        newBuilder Proxy = mempty
        appendBuilder Proxy b c = b <> TextBuilder.singleton c
        finishBuilder = TextBuilder.toLazyText
        unpack = LazyText.unpack
        isEmpty = LazyText.null


    instance HasBuilder ByteString where
        type Token ByteString = Word8
        type Builder ByteString = BytesBuilder.Builder

        newBuilder Proxy = mempty
        appendBuilder Proxy b c = b <> BytesBuilder.word8 c
        finishBuilder = LazyBytes.toStrict . BytesBuilder.toLazyByteString
        unpack = Bytes.unpack
        isEmpty = Bytes.null

    instance HasBuilder LazyBytes.ByteString where
        type Token LazyBytes.ByteString = Word8
        type Builder LazyBytes.ByteString = BytesBuilder.Builder

        newBuilder Proxy = mempty
        appendBuilder Proxy b c = b <> BytesBuilder.word8 c
        finishBuilder = BytesBuilder.toLazyByteString
        unpack = LazyBytes.unpack
        isEmpty = LazyBytes.null

